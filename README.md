Adflecto iOS SDK
==================================
Modified: May 19, 2015  
SDK Version: 1.0.0 

To Download:
----------------------------------
Please go to the [Download](https://bitbucket.org/adflecto/adflecto-ios-sdk/downloads) section to obtain the latest version of Adflecto SDK for iOS platform.


Contains:
----------------------------------
* DemoApps - a coupe of simple demo applications that shows how to integrate with Adflecto SDK:
* Library - latest version of Adfleco SDK


DemoApps:
----------------------------------
* SimpleDemo - a very simpe singe view app that shows how to add Adflecto SDK into your code
* FlappyRect - a simple "Flappy bird" clone game that shows an example of integration into real app

Getting Started with SDK:
----------------------------------
All users are reccommended to review this [documentation](https://bitbucket.org/adflecto/adflecto-ios-sdk/wiki/Home) that describes how to add Adflecto SDK into your applictions.


Legal Requirements:
----------------------------------
By downloading the adflecto SDK, you consent to comply with limited, non-commercial license to use and review the SDK - solely for evaluation purposes. If you wish to integrate this SDK into any commercial application, you must contact adflecto via email [publishers@adflecto.ru](mailto:publishers@adflecto.ru) to sign adflecto publisher agreement and become registered adflecto publisher.

Contact Us:
----------------------------------
For more information, please visit [adflecto.ru](http://adflecto.ru). In case of any question, please email us at [publishers@adflecto.ru](mailto:publishers@adflecto.ru)